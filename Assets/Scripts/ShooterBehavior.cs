﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterBehavior : MonoBehaviour
{
    public GameObject bulletPrefab;
    public int pointValue = 100;
    public float attackDelay = 5f;
    public float attackTimer = 0.0f;
    public bool IsMoving = false;
    public float moveSpeed = 5f;
    public float moveTime = 1f;
    public AudioSource soundPlayer;
    public AudioClip shootSound;
    public AudioClip deathSound;
    public Rigidbody2D rb;
    public bool IsAlive = true;
    public bool IsOnScreen = false;

    private GameObject player;
    private Rigidbody2D shooterTransform;
    private float moveTimer;
    private bool moveDir = true;
    private ParticleSystem explosion;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet") && IsAlive)
        {
            soundPlayer.PlayOneShot(deathSound);
            IsAlive = false;
            ScoreManager.AddPoints(pointValue);
            if (explosion) explosion.Play();
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            StartCoroutine("DestroySelf", 5f);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        shooterTransform = gameObject.GetComponent<Rigidbody2D>();
        IsOnScreen = GetComponent<Renderer>().isVisible;
        moveTimer = moveTime;
        explosion = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }

        if (IsMoving)
        {
            moveTimer -= Time.deltaTime;

            if (moveTimer < 0)
            {
                moveDir = !moveDir;
                moveTimer = moveTime;
            }

            if (moveDir)
            {
                rb.MovePosition(rb.position + Vector2.down * moveSpeed * Time.fixedDeltaTime);
            }
            else
            {
                rb.MovePosition(rb.position + Vector2.up * moveSpeed * Time.fixedDeltaTime);
            }
        }
    }

    private void FixedUpdate()
    {
        if (attackTimer <= 0 && IsAlive && IsOnScreen)
        {
            Vector2 shooterPosition = new Vector2(shooterTransform.position.x, shooterTransform.position.y);
            Vector2 playerDirection = player.GetComponent<Rigidbody2D>().position - shooterPosition;
            //playerDirection = playerDirection.normalized;
            GameObject bulletObject = Instantiate(bulletPrefab, shooterPosition, Quaternion.identity);
            bulletObject.GetComponent<BulletBehavior>().direction = playerDirection.normalized;
            attackTimer = attackDelay;
            soundPlayer.PlayOneShot(shootSound);
        }
    }

    void OnBecameInvisible()
    {
        IsOnScreen = false;
    }

    void OnBecameVisible()
    {
        IsOnScreen = true;
    }

    private IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
