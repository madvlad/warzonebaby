﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BabyBehavior : MonoBehaviour
{
    public Rigidbody2D rb;

    public float flyTimer = 0.25f;

    // Start is called before the first frame update
    void FixedUpdate()
    {
        if (flyTimer > 0)
        {
            rb.MovePosition(rb.position + Vector2.up * 3f * Time.fixedDeltaTime);
            flyTimer -= Time.fixedDeltaTime;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Barrier") || collision.gameObject.CompareTag("WatterBarrier"))
        {
            flyTimer = 0;
            rb.MovePosition(rb.position + Vector2.down * 1f * Time.fixedDeltaTime);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        var bottom = Camera.main.ViewportToWorldPoint(Vector3.zero).y;
        if (rb.position.y + transform.localScale.y / 2 <= bottom)
        {
            SceneManager.LoadScene("GameOverScene");
            GameOverScript.lossReason = GameOverScript.LEFT_BABY_BEHIND;
        }
    }
}
