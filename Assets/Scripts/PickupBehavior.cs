﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBehavior : MonoBehaviour
{
    public string PickupName;
    public bool RequireBaby = false;
    public AudioSource audioPlayer;
    public AudioClip pickupSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (RequireBaby)
            {
                if (!collision.gameObject.GetComponent<PlayerMovement>().hasBaby)
                {
                    return;
                }
            }
            audioPlayer.Stop();
            collision.gameObject.SendMessage("Pickup" + PickupName);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            audioPlayer.PlayOneShot(pickupSound);
            StartCoroutine("DestroyLater");
        }
    }

    private IEnumerator DestroyLater()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
