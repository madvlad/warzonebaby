﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public bool resetScore = true;
    public void LoadScene(string sceneName)
    {
        if(resetScore)
        {
            ScoreManager.ResetScore();
        }
        SceneManager.LoadScene(sceneName);
    }
}
