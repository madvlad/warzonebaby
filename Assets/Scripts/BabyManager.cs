﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BabyManager : MonoBehaviour
{
    public float MaxBabyHealth = 30f;
    public float BabyHealth = 30f;
    public PlayerMovement playerObject;
    public Slider hungerBar;
    public GameObject BabyNotification;

    private AudioSource music;

    public void Start()
    {
        GameObject musicGo = GameObject.FindGameObjectWithTag("Music");

        if (musicGo)
        {
            music = musicGo.GetComponent<AudioSource>();
        }
    }

    public void Feed()
    {
        BabyHealth = Mathf.Clamp(BabyHealth + 10f, 0, 30f);
        UpdateHealthText();
    }

    public void UpdateHealthText()
    {
        float healthPercent = Mathf.RoundToInt(BabyHealth / MaxBabyHealth * 100);

        if (healthPercent < 30 && music.pitch == 1)
        {
            music.pitch = 1.25f;
        }
        else if (healthPercent >= 30 && music.pitch > 1)
        {
            music.pitch = 1;
        }

        hungerBar.value = BabyHealth / MaxBabyHealth;
    }

    private void FixedUpdate()
    {
        if (playerObject.hasBaby)
        {
            BabyNotification.SetActive(false);
            BabyHealth -= Time.fixedDeltaTime;
        }
        else
        {
            BabyNotification.SetActive(true);
            BabyHealth -= Time.fixedDeltaTime * 2;
        }

        if (BabyHealth < 0)
        {
            Cursor.visible = true;
            GameOverScript.lossReason = GameOverScript.BABY_STARVED;
            SceneManager.LoadScene("GameOverScene");
        }

        UpdateHealthText();
    }
}
