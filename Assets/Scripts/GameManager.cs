﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool paused = false;
    public AudioSource pausedMusic;
    public AudioClip bossMusic;
    public GameObject pauseCanvas;
    public GameObject boss;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!paused)
            {
                pauseCanvas.SetActive(true);
                pausedMusic.Play();
                Time.timeScale = 0f;

                GameObject[] audioObjects = GameObject.FindGameObjectsWithTag("Music");
                GameObject babyObject = GameObject.FindGameObjectWithTag("Baby");

                foreach (GameObject go in audioObjects)
                {
                    go.GetComponent<AudioSource>().Pause();
                }

                if (babyObject)
                {
                    babyObject.GetComponent<AudioSource>().Pause();
                }

            }
            else
            {
                pauseCanvas.SetActive(false);
                pausedMusic.Stop();
                Time.timeScale = 1f;
                Cursor.visible = false;

                GameObject[] audioObjects = GameObject.FindGameObjectsWithTag("Music");
                GameObject babyObject = GameObject.FindGameObjectWithTag("Baby");

                foreach(GameObject go in audioObjects)
                {
                    go.GetComponent<AudioSource>().Play();
                }

                if (babyObject)
                {

                    babyObject.GetComponent<AudioSource>().Play();
                }

            }

            paused = !paused;
        }
    }

    public void StartBoss()
    {
        var music = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
        music.Stop();
        music.clip = bossMusic;
        music.Play();

        boss.GetComponent<BossBehavior>().Activate();
    }

    public void EndBoss()
    {
        var music = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
        music.Stop();
    }
}
