﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float attackDelay = 0.25f;
    public float attackTimer = 0.0f;
    public float invulnerabilityTimer = 0.75f;
    public float gunFireCap = 1.5f;
    public bool hasBaby = true;
    public Rigidbody2D rb;
    public Animator animator;
    public AudioClip gunSound;
    public AudioClip machineGunSound;
    public AudioClip hurtSound;
    public GameObject babyPrefab;
    public bool HasMachineGunPickup = false;
    public float MachineGunTimer = 5f;
    public float MachineGunTime = 0f;
    public bool HasShieldPowerUp = false;

    Vector2 movement;
    bool attacked = false;
    AudioSource playerSounds;
    GameManager gm;
    BabyManager bm;
    Rigidbody2D playerRb;
    float invulnTimer = 0.0f;
    private float gunFireTimer = 0f;

    void Start()
    {
        playerSounds = gameObject.GetComponent<AudioSource>();
        playerRb = gameObject.GetComponent<Rigidbody2D>();
        GameObject managers = GameObject.FindGameObjectWithTag("Managers");
        gm = managers.GetComponent<GameManager>();
        bm = managers.GetComponent<BabyManager>();
    }

    void Update()
    {
        if (gm.paused) return;

        this.invulnTimer -= Time.deltaTime;
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
        gunFireTimer -= Time.fixedDeltaTime;
        SetDirection(movement);

        if (this.HasMachineGunPickup)
        {
            if (Input.GetButton("Fire1") && !attacked)
            {
                if (playerSounds != null)
                {
                    playerSounds.PlayOneShot(machineGunSound);
                }
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1") && gunFireTimer < 0)
            {

                if (playerSounds != null)
                {
                    playerSounds.PlayOneShot(gunSound);
                }
                gunFireTimer = gunFireCap;
            }
        }
    }

    void FixedUpdate()
    {
        if (gm.paused) return;
        if (attackTimer > 0) return;

        rb.MovePosition(rb.position + movement.normalized * moveSpeed * Time.fixedDeltaTime);
    }

    void SetDirection(Vector2 movement)
    {
        if (movement.y > 0)
        {
            animator.SetInteger("Direction", 0);
        }

        if (movement.y < 0)
        {
            animator.SetInteger("Direction", 2);
        }

        if (movement.x > 0)
        {
            animator.SetInteger("Direction", 1);
        }

        if (movement.x < 0)
        {
            animator.SetInteger("Direction", 3);
        }
    }

    public void TakeDamage()
    {
        if (this.invulnTimer > 0) return;
        if (!this.hasBaby)
        {
            Cursor.visible = true;
            GameOverScript.lossReason = GameOverScript.SHOT_BY_ENEMY;
            SceneManager.LoadScene("GameOverScene");
        }

        if (this.hasBaby)
        {
            this.invulnTimer = this.invulnerabilityTimer;
            Vector2 babyPosition = new Vector2(playerRb.position.x, playerRb.position.y);
            babyPosition += Vector2.up * 2;
            this.hasBaby = false;
            this.gameObject.GetComponent<PointAndShoot>().HasBaby = false;
            Instantiate(babyPrefab, babyPosition, Quaternion.identity);
        }

        if (playerSounds != null)
        {
            playerSounds.PlayOneShot(hurtSound);
        }
    }

    public void PickupMachinegun()
    {
        this.HasMachineGunPickup = true;
        StopCoroutine("MachineGunExpire");
        StartCoroutine("MachineGunExpire");
    }

    public void PickupBabyBottle()
    {
        if (bm && hasBaby)
        {
            bm.Feed();
        }
    }

    public void PickupBaby()
    {
        this.hasBaby = true;
        this.gameObject.GetComponent<PointAndShoot>().HasBaby = true;
    }
    
    IEnumerator MachineGunExpire()
    {
        yield return new WaitForSeconds(MachineGunTimer);
        this.HasMachineGunPickup = false;
    }
}
