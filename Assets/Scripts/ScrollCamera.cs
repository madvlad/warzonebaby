﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollCamera : MonoBehaviour
{
    public float cameraSpeed = 0.005f;
    public float stopScrollYValue;

    private bool notifyOfBoss = true;
    private GameManager gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Managers").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Camera.main.transform.position.y >= stopScrollYValue)
        {
            if (notifyOfBoss)
            {
                gm.StartBoss();
                notifyOfBoss = false;
                // Tell GameManager that Boss is ready!
            }
        }
        else
        {
            Camera.main.transform.position = Camera.main.transform.position + Vector3.up * cameraSpeed;
        }
    }
}
