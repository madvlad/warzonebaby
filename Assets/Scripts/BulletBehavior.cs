﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    public float bulletSpeed = 2.5f;
    public Rigidbody2D rb;
    public Vector2 direction = Vector2.down;
    public bool PlayerBullet;

    private void Start()
    {
        StartCoroutine("DestroySelf", 5f);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + direction * bulletSpeed * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Barrier"))
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Player") && !PlayerBullet)
        {
            collision.gameObject.SendMessage("TakeDamage");
            Destroy(gameObject);
        }

        if (PlayerBullet && collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
