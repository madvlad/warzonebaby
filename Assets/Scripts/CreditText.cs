﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditText : MonoBehaviour
{
    public GameObject WaveTextObj;
    public GameObject textPrefab;

    private void Start()
    {
        StartCoroutine("WaveEffect", "Bullets & Babies");
    }

    IEnumerator WaveEffect(string text)
    {
        //clear out the existing text

        while (WaveTextObj.transform.childCount > 0)
        {
            Destroy(WaveTextObj.transform.GetChild(0));
        }

        //create new text
        float scalar = 0.1f;
        float timeScalar = 3;
        for (int i = 0; i < text.Length; i++)
        {
            GameObject o = Instantiate(textPrefab, new Vector3(i * scalar, 0, 0), Quaternion.identity, WaveTextObj.transform);
           o.GetComponent<Text>().text = text[i].ToString();
            
        }
        //animate text
        while (true)
        {
            Debug.Log("Yes");
            int i = 0;
            foreach (Transform child in transform)
            {
                Vector3 p = child.localPosition;
                // By setting each text object's y position to a value controlled by a sine wave,
                // they will jiggle up and down. Their index in the string handles each letter being
                // at a different point on the wave.
                p.y = Mathf.Sin((Time.time + i * scalar) * timeScalar) * scalar;
                child.localPosition = p;
                i++;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
