﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private static int ScoreValue = 0;

    public Text scoreText;
    public string scoreLabel = "Score: ";
    

    // Update is called once per frame
    void Update()
    {
        scoreText.text = scoreLabel + ScoreValue;
    }

    public static void AddPoints(int points)
    {
        ScoreValue += points;
    }

    public static void ResetScore()
    {
        ScoreValue = 0;
    }

    public static int GetScore()
    {
        return ScoreValue;
    }
}
