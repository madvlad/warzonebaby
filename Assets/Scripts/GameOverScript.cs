﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    public Text lossText;

    public const string LEFT_BABY_BEHIND = "You left the baby behind!";
    public const string BABY_STARVED = "You let the baby starve!";
    public const string SHOT_BY_ENEMY = "You died!";

    public static string lossReason;
    public void Start()
    {
        lossText.text = lossReason;
    }
}
