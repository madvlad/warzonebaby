﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cutscene_LoadLater : MonoBehaviour
{
    public string Label;
    public string SceneToLoad;
    public float waitTime = 2f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("StartLoad");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator StartLoad()
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene(SceneToLoad);
    }
}
