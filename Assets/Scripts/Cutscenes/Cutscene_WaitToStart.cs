﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscene_WaitToStart : MonoBehaviour
{
    public string Label;
    public GameObject goToEnable;
    public float waitTime = 6f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("StartText");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator StartText()
    {
        yield return new WaitForSeconds(waitTime);
        goToEnable.SetActive(true);
    }
}
