﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyShooterBehavior : MonoBehaviour
{
    public GameObject bulletPrefab;
    public int pointValue = 100;
    public float attackDelay = 5f;
    public float attackTimer = 0.0f;
    public bool IsMoving = false;
    public float moveSpeed = 5f;
    public float moveTime = 1f;
    public AudioSource soundPlayer;
    public AudioClip shootSound;
    public AudioClip deathSound;
    public Rigidbody2D rb;

    private GameObject player;
    private Rigidbody2D shooterTransform;
    private float moveTimer;
    private bool moveDir = true;
    private ParticleSystem explosion;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        shooterTransform = gameObject.GetComponent<Rigidbody2D>();
        moveTimer = moveTime;
        explosion = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        if (attackTimer <= 0)
        {
            float random = Random.Range(0f, 260f);
            Vector2 shooterPosition = new Vector2(shooterTransform.position.x, shooterTransform.position.y);
            Vector2 shotDirection = new Vector2(Mathf.Cos(random), Mathf.Sin(random));
            //playerDirection = playerDirection.normalized;
            GameObject bulletObject = Instantiate(bulletPrefab, shooterPosition, Quaternion.identity);
            bulletObject.GetComponent<BulletBehavior>().direction = shotDirection.normalized;
            attackTimer = attackDelay;
            soundPlayer.PlayOneShot(shootSound);
        }
    }

    private IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
