﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBehavior : MonoBehaviour
{
    public bool IsActive = false;
    public int Health = 30;
    public float attackDelay = 1f;
    public float attackTimer = 0.0f;
    public AudioSource sound;
    public AudioClip die;
    public AudioClip shoot;
    public AudioClip damage;
    public BoxCollider2D goalTrigger;
    public GameObject bulletPrefab;
    public Slider bossHealth;
    public GameObject bossHealthContainer;

    private GameObject player;
    private Rigidbody2D shooterTransform;
    private ParticleSystem explosion;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        shooterTransform = gameObject.GetComponent<Rigidbody2D>();
        explosion = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsActive) return;
        attackTimer -= Time.fixedDeltaTime;
    }

    private void FixedUpdate()
    {
        if (!IsActive) return;

        if (attackTimer <= 0)
        {
            Vector2 shooterPosition = new Vector2(shooterTransform.position.x, shooterTransform.position.y);
            Vector2 playerDirection = player.GetComponent<Rigidbody2D>().position - shooterPosition;
            //playerDirection = playerDirection.normalized;

            Vector2 bullet1pos = new Vector2(shooterPosition.x - 1, shooterPosition.y - 1);
            Vector2 bullet2pos = new Vector2(shooterPosition.x + 1, shooterPosition.y - 1);

            GameObject bulletObject = Instantiate(bulletPrefab, bullet1pos, Quaternion.identity);
            GameObject bulletObject2 = Instantiate(bulletPrefab, bullet2pos, Quaternion.identity);

            bulletObject.GetComponent<BulletBehavior>().direction = playerDirection.normalized;
            bulletObject2.GetComponent<BulletBehavior>().direction = playerDirection.normalized;

            attackTimer = attackDelay;
            sound.PlayOneShot(shoot);
        }
    }

    public void Activate()
    {
        this.IsActive = true;
        this.bossHealthContainer.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsActive) return;

        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            Destroy(collision.gameObject);
            Health--;
            sound.PlayOneShot(damage);
            this.bossHealth.value = Health;
        }

        if (Health == 0)
        {
            Die();
        }
    }

    private void Die()
    {
        sound.PlayOneShot(die);
        this.IsActive = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        GameObject.FindGameObjectWithTag("Managers").GetComponent<GameManager>().EndBoss();
        goalTrigger.enabled = true;
        explosion.Play();
        this.bossHealthContainer.SetActive(false);
    }
}
